class CalculateIAQ {
	constructor() {}
	init({ co2, humidity, temperature, voc }) {

		const co2_index = this.calculateCo2Index(co2);
		const comfort_index = this.calculateComfortIndex({ humidity, temperature });
		const voc_index = this.calculateVocIndex(voc);

		return {
   co2_index,
   comfort_index,
   voc_index,
   iaq_index:Math.max(co2_index, comfort_index, voc_index)
  };
	}
	calculateCo2Index(co2) {
		const co2Object = {
			"0-400": 1,
			"400-1000": 2,
			"1000-1500": 3,
			"1500-2000": 4,
			"2000-5000": 5,
			"5000-10000": 6,
		};
		for (const key in co2Object) {
			const rangeSplit = key.split("-");
			const min = parseInt(rangeSplit[0]);
			const max = parseInt(rangeSplit[1]);
			if (co2 >= min && co2 <= max) {
				return co2Object[key];
			}
		}
	}

	calculateComfortIndex({ humidity, temperature }) {
		const tempHumidityObj = {
			"10-16": 6,
			"10-17": 6,
			"10-18": 6,
			"10-19": 6,
			"10-20": 6,
			"10-21": 6,
			"10-22": 6,
			"10-23": 6,
			"10-24": 6,
			"10-25": 6,
			"10-26": 6,
			"10-27": 6,
			"10-28": 6,

			"20-16": 6,
			"20-17": 5,
			"20-18": 5,
			"20-19": 5,
			"20-20": 5,
			"20-21": 5,
			"20-22": 5,
			"20-23": 5,
			"20-24": 5,
			"20-25": 5,
			"20-26": 5,
			"20-27": 5,
			"20-28": 6,

			"30-16": 6,
			"30-17": 5,
			"30-18": 5,
			"30-19": 4,
			"30-20": 4,
			"30-21": 4,
			"30-22": 4,
			"30-23": 4,
			"30-24": 4,
			"30-25": 4,
			"30-26": 4,
			"30-27": 5,
			"30-28": 6,

			"40-16": 6,
			"40-17": 5,
			"40-18": 5,
			"40-19": 4,
			"40-20": 3,
			"40-21": 3,
			"40-22": 3,
			"40-23": 2,
			"40-24": 2,
			"40-25": 3,
			"40-26": 4,
			"40-27": 5,
			"40-28": 6,

			"50-16": 5,
			"50-17": 5,
			"50-18": 4,
			"50-19": 3,
			"50-20": 2,
			"50-21": 1,
			"50-22": 1,
			"50-23": 1,
			"50-24": 2,
			"50-25": 3,
			"50-26": 4,
			"50-27": 5,
			"50-28": 6,

			"60-16": 5,
			"60-17": 4,
			"60-18": 3,
			"60-19": 2,
			"60-20": 1,
			"60-21": 1,
			"60-22": 1,
			"60-23": 2,
			"60-24": 2,
			"60-25": 3,
			"60-26": 4,
			"60-27": 5,
			"60-28": 6,

			"70-16": 5,
			"70-17": 4,
			"70-18": 3,
			"70-19": 2,
			"70-20": 1,
			"70-21": 1,
			"70-22": 1,
			"70-23": 2,
			"70-24": 2,
			"70-25": 3,
			"70-26": 4,
			"70-27": 5,
			"70-28": 6,

			"80-16": 5,
			"80-17": 4,
			"80-18": 2,
			"80-19": 2,
			"80-20": 2,
			"80-21": 2,
			"80-22": 2,
			"80-23": 3,
			"80-24": 4,
			"80-25": 5,
			"80-26": 5,
			"80-27": 5,
			"80-28": 6,

			"90-16": 6,
			"90-17": 5,
			"90-18": 4,
			"90-19": 3,
			"90-20": 3,
			"90-21": 3,
			"90-22": 3,
			"90-23": 4,
			"90-24": 5,
			"90-25": 5,
			"90-26": 6,
			"90-27": 6,
			"90-28": 6,
		};

		for (const key in tempHumidityObj) {
			const keySplit = key.split("-");
			const hum = parseInt(keySplit[0]);
			const temp = parseInt(keySplit[1]);
			if (Math.round(Math.floor(humidity)/10)*10 === hum && Math.round(temperature) === temp) {
				return tempHumidityObj[key];
			}
		}
	}
	calculateVocIndex(voc) {
		const vocObject = {
			"0-50": 1,
			"51-100": 2,
			"101-150": 3,
			"151-200": 4,
			"201-300": 5,
			"301-500": 6,
		};
		for (const key in vocObject) {
			const rangeSplit = key.split("-");
			const min = parseInt(rangeSplit[0]);
			const max = parseInt(rangeSplit[1]);
			if (voc >= min && voc <= max) {
				return vocObject[key];
			}
		}
	}
}

module.exports.CalculateIAQ = CalculateIAQ;
