
const mqtt = require("mqtt");
const mqttHost = process.env.MQTT_HOST || 'mqtt://192.168.1.176:1883';

class Mqtt {
    constructor() {
        this.client = mqtt.connect(mqttHost);
    }
    connect() {
        return new Promise((res, rej) =>
            this.client.on("connect", () => {
                
                res(true);
            })
        );
    }
    subscribe(topic) {
        this.client.subscribe(topic, { qos: 2 }, (error) => {
            if (!error) {
                console.log(topic);
            }
        });
    }
    message(callback) {
        this.client.on("message", (topic, message) => {
            //    console.log({topic,message:message.toString()});
            callback(message.toString());
            //this.client.end()
        });
    }
}

module.exports.Mqtt=Mqtt
